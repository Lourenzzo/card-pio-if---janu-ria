<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrCardapio */

$this->title = 'Update Jr Cardapio: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jr Cardapios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jr-cardapio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
