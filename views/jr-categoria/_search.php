<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JrCategoriaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jr-categoria-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Salada') ?>

    <?= $form->field($model, 'Pratoprincipal') ?>

    <?= $form->field($model, 'Carne') ?>

    <?= $form->field($model, 'Sobremesa') ?>

    <?= $form->field($model, 'Suco') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
