<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JrCategoria */

$this->title = 'Update Jr Categoria: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jr Categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jr-categoria-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
