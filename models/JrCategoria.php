<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jr_categoria".
 *
 * @property string $Salada
 * @property string $Pratoprincipal
 * @property string $Carne
 * @property string $Sobremesa
 * @property string $Suco
 * @property int $id
 *
 * @property JrPratos[] $jrPratos
 */
class JrCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jr_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Salada', 'Carne', 'Sobremesa', 'Suco'], 'string', 'max' => 20],
            [['Pratoprincipal'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Salada' => 'Salada',
            'Pratoprincipal' => 'Prato principal',
            'Carne' => 'Carne',
            'Sobremesa' => 'Sobremesa',
            'Suco' => 'Suco',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJrPratos()
    {
        return $this->hasMany(JrPratos::className(), ['categoria_id' => 'id']);
    }
}
